import React, { useState } from 'react'

interface Props {
	selectedDate: Date
	onChange: (date: Date) => void
}

const HandmadeDatePicker: React.FC<Props> = ({ selectedDate, onChange }) => {
	const [isOpen, setIsOpen] = useState(false)
	const [selectedMonth, setSelectedMonth] = useState(selectedDate.getMonth())
	const [selectedYear, setSelectedYear] = useState(selectedDate.getFullYear())

	const daysInMonth = (month: number, year: number) => {
		return new Date(year, month + 1, 0).getDate()
	}

	const handleMonthChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
		setSelectedMonth(parseInt(e.target.value))
	}

	const handleYearChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
		setSelectedYear(parseInt(e.target.value))
	}

	const handleDateClick = (day: number) => {
		const newDate = new Date(selectedYear, selectedMonth, day)
		onChange(newDate)
		setIsOpen(false)
	}

	const renderCalendar = () => {
		const days = daysInMonth(selectedMonth, selectedYear)
		const firstDay = new Date(selectedYear, selectedMonth, 1).getDay()
		const rows = []

		let day = 1
		for (let i = 0; i < 6; i++) {
			const columns = []
			for (let j = 0; j < 7; j++) {
				if (i === 0 && j < firstDay) {
					columns.push(<td key={`${i}-${j}`} />)
				} else if (day <= days) {
					columns.push(
						<td key={`${i}-${j}`} onClick={() => handleDateClick(day)}>
							{day}
						</td>
					)
					day++
				} else {
					columns.push(<td key={`${i}-${j}`} />)
				}
			}
			rows.push(<tr key={i}>{columns}</tr>)
		}

		return (
			<table>
				<thead>
					<tr>
						<th colSpan={7}>
							<select value={selectedMonth} onChange={handleMonthChange}>
								<option value={0}>January</option>
								<option value={1}>February</option>
								<option value={2}>March</option>
								<option value={3}>April</option>
								<option value={4}>May</option>
								<option value={5}>June</option>
								<option value={6}>July</option>
								<option value={7}>August</option>
								<option value={8}>September</option>
								<option value={9}>October</option>
								<option value={10}>November</option>
								<option value={11}>December</option>
							</select>
							<select value={selectedYear} onChange={handleYearChange}>
								<option value={selectedYear - 1}>{selectedYear - 1}</option>
								<option value={selectedYear}>{selectedYear}</option>
								<option value={selectedYear + 1}>{selectedYear + 1}</option>
							</select>
						</th>
					</tr>
					<tr>
						<th>Sun</th>
						<th>Mon</th>
						<th>Tue</th>
						<th>Wed</th>
						<th>Thu</th>
						<th>Fri</th>
						<th>Sat</th>
					</tr>
				</thead>
				<tbody>{rows}</tbody>
			</table>
		)
	}

	return (
		<div>
			<input
				type='text'
				value={selectedDate.toLocaleDateString()}
				onFocus={() => setIsOpen(true)}
				onBlur={() => setIsOpen(false)}
				readOnly
			/>
			{isOpen && <div>{renderCalendar()}</div>}
		</div>
	)
}

export default HandmadeDatePicker
