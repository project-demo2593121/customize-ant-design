/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { FunctionComponent } from 'react'
import { DatePicker } from 'antd'
import 'dayjs/locale/vi'
import locale from 'antd/es/date-picker/locale/vi_VN'
import dayjs from 'dayjs'

import './style.css'
import { getLunarDate } from '../../utils/convertSolarToLunar'
import { RangePickerProps } from 'antd/es/date-picker'

const { RangePicker } = DatePicker

// (property) RangePicker: PickerComponentClass<RangePickerProps<dayjs.Dayjs> & {
//     dropdownClassName?: string | undefined;
//     popupClassName?: string | undefined;
// }, unknown>

const LunarRangePicker: FunctionComponent<RangePickerProps> = (props) => {
	return (
		<RangePicker
			popupClassName='popup-date-picker'
			locale={locale}
			dateRender={(current: dayjs.Dayjs): React.ReactNode => {
				const style: React.CSSProperties = {}
				style.width = '46px'
				style.height = '46px'

				const currentDate = dayjs(current).get('date')
				const currentMonth = dayjs(current).get('month')
				const currentYear = dayjs(current).get('year')

				const currentLunarDate = getLunarDate(currentDate, currentMonth + 1, currentYear)
				const { day, month } = currentLunarDate

				return (
					<div className='ant-picker-cell-inner' style={style}>
						<span className='solar-day'>{current.date()}</span>
						{day === 1 ? (
							<span className='lunar-day' style={{ color: 'red' }}>
								{day}/{month}
							</span>
						) : (
							<span className='lunar-day'>{day}</span>
						)}
					</div>
				)
			}}
			{...props}
		/>
	)
}

export default LunarRangePicker
