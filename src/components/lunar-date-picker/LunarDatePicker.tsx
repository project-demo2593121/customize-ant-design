/* eslint-disable no-case-declarations */
import React, { FC, ReactNode, useCallback } from 'react'
import { DatePickerProps } from 'antd'
import dayjs, { Dayjs } from 'dayjs'
import 'dayjs/locale/vi'

import { getLunarDate } from '../../utils/convertSolarToLunar'
import { StyledDatePicker } from './LunarDatePickerStyles'

const LunarDatePicker: FC<DatePickerProps> = (props) => {
	const renderAntPickerCell = useCallback((current: Dayjs, type: string) => {
		const currentDate = dayjs(current).get('date')
		const currentMonth = dayjs(current).get('month') + 1
		const currentYear = dayjs(current).get('year')

		switch (type) {
			case 'date':
				const currentLunarDate = getLunarDate(currentDate, currentMonth, currentYear)
				const { day, month } = currentLunarDate
				return (
					<div className='ant-picker-cell-inner'>
						<span className='solar-day'>{current.date()}</span>
						{day === 1 ? (
							<span className='lunar-day highlight'>
								{day}/{month}
							</span>
						) : (
							<span className='lunar-day'>{day}</span>
						)}
					</div>
				)

			case 'month':
				return <div className='ant-picker-cell-inner'>Th{`0${currentMonth}`.slice(-2)}</div>

			case 'year':
				return <div className='ant-picker-cell-inner'>{currentYear}</div>
		}
	}, [])

	return (
		<StyledDatePicker
			format='DD/MM/YYYY'
			getPopupContainer={(trigger: any) => trigger}
			cellRender={(current: Dayjs, today: { type: string }): ReactNode =>
				renderAntPickerCell(current, today.type)
			}
			{...props}
		/>
	)
}

export default LunarDatePicker
