import { DatePicker, DatePickerProps } from 'antd'
import styled from 'styled-components'

type StyledDatePickerProps = DatePickerProps | any
export const StyledDatePicker = styled(DatePicker)<StyledDatePickerProps>`
	--size-cell-inner: 40px;

	&&& {
		.ant-picker-dropdown .ant-picker-cell .ant-picker-cell-inner {
			border-radius: 0px;

			::before {
				border-radius: 0px;
			}
		}

		.ant-picker-date-panel {
			width: auto;

			.ant-picker-body {
				padding-bottom: 15px;
			}

			td.ant-picker-cell.ant-picker-cell-disabled {
				::before {
					content: '';
					background: transparent;
				}
			}

			.ant-picker-cell-inner {
				width: var(--size-cell-inner);
				height: var(--size-cell-inner);

				display: block;
			}

			.ant-picker-date-panel {
				width: auto;
			}

			.ant-picker-content {
				width: auto;
			}

			tbody {
				td {
					border: 1px solid rgba(5, 5, 5, 0.06);
					padding: 0px;
				}
			}

			.solar-day {
				position: absolute;
				left: 4px;
				font-weight: bold;
			}

			.lunar-day {
				position: absolute;
				bottom: 0px;
				right: 4px;
				font-size: 10px;
			}

			.highlight {
				color: red;
				font-weight: bold;
			}

			td.ant-picker-cell.ant-picker-cell-in-view.ant-picker-cell-selected {
				.highlight {
					color: white;
				}
			}
		}
	}
`
